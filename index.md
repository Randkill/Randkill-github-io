
## About Randkill

Randkill is a Bachelor Science Student in Computer Science. A young developer who tries to learn new stuff, also creates his own.

## Interests & Skills

His interests are in Back-End developing, AI(Artificial Intelligence), ML(Machine learning), NN(Neural Networks), Image Processing , etc.
Also well known and formal to C++, Java, Android(native), JavaScript, NodeJS and low-level introduced to Python.

## Contact Randkill

You could be in touch with him via his email address : randkill.randkill@gmail.com
and also his [GitHub-Page](https://github.com/Randkill)
